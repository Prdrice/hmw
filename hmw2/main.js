"use strict";

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

class FieldError extends Error {
  constructor(value) {
    super();
    this.message = `${value} is not valid`;
    this.name = "FieldError";
  }
}

class Book {
  constructor(key, value) {
    this.key = key;
    this.value = value;
    this.item = document.createElement("li");
    this.listWrapper = document.createElement("ul");
  }
  createElement() {
    this.item.innerText = `${this.key} : ${this.value}`;
    this.listWrapper.append(this.item);
  }
  render(container = document.body) {
    this.createElement();
    container.append(this.listWrapper);
  }
}

books.forEach((book) => {
  try {
    // if (!book.author) {
    //   throw new FieldError(`"auther"`);
    // } else if (!book.name) {
    //   throw new FieldError(`"name"`);
    // } else if (!book.price) {
    //   throw new FieldError(`"price"`);
    // }

    // for (let key in book) {
    //   new Book(key, book[key]).render(document.querySelector("#root"));
    // }

    // check if book and book's necessary fields are present,
    // else throw an error
    
    if (book && book.author && book.name && book.price) {
      // iterate over each key in the book object
      for (let key in book) {
        // render new Book to #root DOM element
        new Book(key, book[key]).render(document.querySelector("#root"));
      }
    } else {
      // throw FieldError with appropriate message
      throw new FieldError(`"author", "name", "price"`);
    }

  } catch (err) {
    if (err.name === "FieldError") {
      console.log(err);
    } else {
      throw err;
    }
  }
});
