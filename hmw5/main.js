
const container = document.querySelector(".container");

const getData = async () => {
  try {
    const usersApi = await fetch("https://ajax.test-danit.com/api/json/users")
      .then((response) => response.json())
      .then((dataUser) => dataUser);
    console.log(usersApi);
    const postsApi = await fetch("https://ajax.test-danit.com/api/json/posts")
      .then((response) => response.json())
      .then((dataPosts) => dataPosts);

    postsApi.forEach(({ id: postId, title, body, userId }) => {
      const user = usersApi.find((user) => user.id === userId);
      new UserCard(
        user.id,
        user.name,
        user.email,
        user.username,
        postId,
        title,
        body,
        userId
      ).render(container);
    });
  } catch (err) {
    console.error(err);
  }
};
getData();

