class UserCard {
         constructor(id, name, email, username, postId, title, body, userId) {
           this.postId = postId;
           this.userId = userId;
           this.name = name;
           this.id = id;
           this.username = username;
           this.email = email;
           this.body = body;
           this.title = title;
           this.card = document.createElement("div");
           this.deleteButton = document.createElement("button");
       
           this.deleteButton.addEventListener("click", () => {
             fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
               method: "DELETE",
               headers: { "Content-Type": "application/json" },
               body: JSON.stringify(),
             })
               .then((response) => response.json)
               .then((result) => this.card.remove());
           });
         }
       
         createElements() {
           this.deleteButton.innerHTML = "DELETE";
           this.deleteButton.setAttribute("id", `${this.postId}`);
           this.deleteButton.classList.add("del-button");
         
           this.card.classList.add("card");
           this.card.insertAdjacentElement("afterbegin", this.deleteButton);
           this.card.insertAdjacentHTML(
             "beforeend",
             `<div class = "user-nickname">${this.username}</div>
               <p class = "user-data">${this.name} <a class = "user-link" href ="#">${this.email}</a></p>
               <h4 class = "post-title">${this.title}</h4>
               <p class = "post-body">${this.body}</p>`
           );
         
         }
         
         render(container) {
           this.createElements();
           container.append(this.card);
         } 
       }
       