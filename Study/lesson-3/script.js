// function User(name, age) {
// 	this.name = name;
// 	if (age) {
// 		this.age = age;
// 	}
// 	this.getAge = function () {
// 		return this.age;
// 	};
// }

// const user = {
// 	name: "Uasya",
// 	getname() {
// 		return this.name;
// 	},

// 	get name() {
// 		return this._name;
// 	},

// 	set name(value) {
// 		this._name = value;
// 	},
// };

// user.getname();

// user.name = "Adrey";
// user.name;
// console.log(user);
// const user = new User("Uasya", 29);
// const user2 = new User("Uasya");

// console.log(User.__proto__);
// console.log(User.prototype);
// console.log(user.__proto__);

// console.log(user instanceof User);

// console.log(user);
// console.log(user2);

// User.prototype.getName = function () {
// 	return this.name;
// };

// console.log(user.getName());
// console.log(user2.getName());

// console.log(Object.keys(user));

// for (const key in user) {
// 	if (Object.hasOwn(user, "key")) {
// 		console.log(key);
// 	}
// }

// const baz = () => {};
// const baz = { a: 3 };

// function foo() {}

// foo.prototype = baz;

// const bar = Object.create(baz);

// console.log(bar instanceof foo);

// class User {
// 	status = "active";
// 	#iq = 130;
// 	static MAX_AGE = 120;

// 	constructor(name) {
// 		this.name = name;
// 		// this.status = "active";

// 		this.getStatus = () => {
// 			return this.status;
// 		};
// 	}

// 	static getMaxAge() {
// 		return this.MAX_AGE;
// 	}

// 	getIQ() {
// 		return this.#iq;
// 	}

// 	getName() {
// 		return this.name;
// 	}
// }

// const user = new User("Uasya");

// console.log(user);
// console.log(user.status);
// console.log(user.getIQ());
// console.log(User.MAX_AGE);
// console.log(User.getMaxAge());

// class Guest extends User {
// 	constructor(name, access) {
// 		super(name);

// 		this.access = access;
// 	}

// 	createComment(commentText) {
// 		this.comment = commentText;
// 	}
// }

// const guest = new Guest("Uasya", "just see");
// console.log(guest);
// console.log(guest.getName());

// class Visitor extends Guest {
// 	constructor(name, access, session) {
// 		super(name, access);

// 		this.session = session;
// 	}
// }

// ##TASK 1

// class Patien {
// 	constructor(name, lastName, date, gender) {
// 		this.name = name;
// 		this.lastName = lastName;
// 		this.date = date;
// 		this.gender = gender;
// 	}
// }

// class PatienCard extends Patien {
// 	constructor(name, lastName, date, gender, pressure, problems) {
// 		super(name, lastName, date, gender);
// 		this.pressure = pressure;
// 		this.problems = problems;
// 	}
// }

// class PatienDentist extends Patien {
// 	constructor(name, lastName, date, gender, visit, treatment) {
// 		super(name, lastName, date, gender);
// 		this.visit = visit;
// 		this.treatment = treatment;
// 	}
// }

// let patien = new Patien("Bohdan", "Kravets", "12", "male");
// console.log(patien);

// let dentist = new PatienDentist("Uasya", "Petrov", "15", "male", "23", "toothth");
// console.log(dentist);

//  TASK 2

// class Modal {
// 	constructor(id, classes, text) {
// 		this.id = id;
// 		this.classes = classes;
// 		this.text = text;
// 	}
// 	render = function () {
// 		this.window = document.createElement("div");
// 		this.window.id = this.id;
// 		this.window.classList.add(...this.classes);

// 		const modalContent = document.createElement("div");
// 		modalContent.classList.add("modal-content");

// 		const span = document.createElement("span");
// 		span.classList.add("close");
// 		span.innerHTML = "&times;";

// 		const paragraph = document.createElement("p");
// 		paragraph.textContent = this.text;

// 		modalContent.append(span, paragraph);

// 		this.window.append(modalContent);

// 		span.addEventListener("click", this.closeModal.bind(this));

// 		return this.window;
// 	};
// 	openModal = function () {
// 		this.window.classList.add("active");
// 	};
// 	closeModal = function () {
// 		this.window.classList.remove("active");
// 	};
// }

// const root = document.querySelector("#root");
// const modal = new Modal("window", ["modal", "lorem"], "lorem ipsum dolot");
// root.append(modal.render());

// const btn = document.querySelector("#myBtn");
// btn.addEventListener("click", modal.openModal.bind(modal));

// TASK 3

// class Modal {
//   constructor(id, classes, text) {
//     this.id = id;
//     this.classes = classes;
//     this.text = text;
//   }
//   render = function (content = "") {
//     this.window = document.createElement("div");
//     this.window.id = this.id;
//     this.window.classList.add(...this.classes);
//     const modalContent = document.createElement("div");
//     modalContent.classList.add("modal-content");
//     const span = document.createElement("span");
//     span.classList.add("close");
//     span.innerHTML = "&times;";
//     modalContent.append(span, content);
//     this.window.append(modalContent);

//     span.addEventListener("click", this.closeModal.bind(this));

//     return this.window;
//   };
//   openModal = function () {
//     this.window.classList.add("active");
//   };
//   closeModal = function () {
//     this.window.classList.remove("active");
//   };
// }

// class Registration extends Modal {
//   constructor(id, classes, text) {
//     super(id, classes, text);
//   }
//   createForm = function () {
//     const registrationForm = document.createElement("form");
//     registrationForm.action = "";
//     registrationForm.id = "register-form";

//     const inputLogin = new Input(
//       "text",
//       "login",
//       true,
//       "regLogin",
//       "paper-input",
//       "Ваш логин",
//       "ERROR"
//     );
//     const inputEmail = new Input(
//       "email",
//       "email",
//       true,
//       "regLogin",
//       "paper-input",
//       "Ваш Email",
//       "ERROR"
//     );
//     const inputPassword = new Input(
//       "password",
//       "password",
//       true,
//       "regLogin",
//       "paper-input",
//       "Ваш пароль",
//       "ERROR"
//     );
//     const inputRepeatPassword = new Input(
//       "password",
//       "repeat-password",
//       true,
//       "regLogin",
//       "paper-input",
//       "Повторите  пароль",
//       "ERROR"
//     );
//     const inputSubmit = new Input(
//       "password",
//       "repeat-password",
//       true,
//       "regLogin",
//       "paper-input",
//       "Повторите  пароль",
//       "ERROR"
//     );

//     const inputLogin = document.createElement("input");
//     inputLogin.type = "text";
//     inputLogin.name = "login";
//     inputLogin.placeholder = "Ваш логин";
//     inputLogin.required = "true";

//     const inputEmail = document.createElement("input");
//     inputEmail.type = "email";
//     inputEmail.name = "email";
//     inputEmail.placeholder = "Ваш Email";
//     inputEmail.required = "true";

//     const inputPassword = document.createElement("input");
//     inputPassword.type = "password";
//     inputPassword.name = "password";
//     inputPassword.placeholder = "Ваш пароль";
//     inputPassword.required = "true";

//     const inputRepeatPassword = document.createElement("input");
//     inputRepeatPassword.type = "password";
//     inputRepeatPassword.name = "repeat-password";
//     inputRepeatPassword.placeholder = "Повторите  пароль";
//     inputRepeatPassword.required = "true";

//     const inputSubmit = document.createElement("input");
//     inputSubmit.type = "submit";
//     inputSubmit.value = "Регистрация";

//     registrationForm.append(
//       inputLogin,
//       inputEmail,
//       inputPassword,
//       inputRepeatPassword,
//       inputSubmit
//     );

//     return registrationForm;
//   };
// }

// class Login extends Modal {
//   constructor(id, classes, text) {
//     super(id, classes, text);
//   }
//   createForm = function () {
//     const loginForm = document.createElement("form");
//     loginForm.action = "";
//     loginForm.id = "login-form";

//     const inputLogin = document.createElement("input");
//     inputLogin.type = "text";
//     inputLogin.name = "login";
//     inputLogin.placeholder = "Ваш логин";
//     inputLogin.required = "true";

//     const inputPassword = document.createElement("input");
//     inputPassword.type = "password";
//     inputPassword.name = "password";
//     inputPassword.placeholder = "Ваш пароль";
//     inputPassword.required = "true";

//     const inputSubmit = document.createElement("input");
//     inputSubmit.type = "submit";
//     inputSubmit.value = "Регистрация";

//     loginForm.append(inputLogin, inputPassword, inputSubmit);

//     return loginForm;
//   };
// }

// class Input {
//   constructor(type, name, obligatory, id, classes, placeholder, textError) {
//     this.type = type;
//     this.name = name;
//     this.obligatory = obligatory;
//     this.id = id;
//     this.classes = classes;
//     this.placeholder = placeholder;
//     this.textError = textError;
//   }
//   render() {
//     this.input = document.createElement("input");
//     this.input.setAttribute("type", this.type);
//     this.input.setAttribute("name", this.name);
//     if (this.required === true) {
//       this.input.setAttribute("required", "");
//     }
//     this.input.setAttribute("id", this.id);
//     this.input.setAttribute("class", this.classes);
//     this.input.setAttribute("placeholder", this.placeholder);
//     this.input.addEventListener("blur", this.handleBlur);
//   }
// }

// const root = document.querySelector("#root");
// const modal = new Modal("window", ["modal", "lorem"], "lorem ipsum dolot");
// root.append(modal.render());

// const btn = document.querySelector("#myBtn");
// btn.addEventListener("click", modal.openModal.bind(modal));

// const registrationForm = new Registration(
//   "window",
//   ["modal", "lorem"],
//   "lorem ipsum dolot"
// );
// const registerBtn = document.querySelector("#registration-btn");
// registerBtn.addEventListener(
//   "click",
//   registrationForm.openModal.bind(registrationForm)
// );
// root.append(registrationForm.render(registrationForm.createForm()));

// const loginForm = new Login("window", ["modal", "lorem"], "lorem ipsum dolot");
// const loginBtn = document.querySelector("#login-btn");
// loginBtn.addEventListener("click", loginForm.openModal.bind(loginForm));
// root.append(loginForm.render(loginForm.createForm()));

// ##TASK 3
// Напишите универсальный класс Input, который будет создавать однострочное поле
// ввода. У него будут такие параметры: - тип (text, email, password, number, date,
// submit и т.д.); - name; - обязательное поле или нет; - id; - классы; -
// placeholder; - errorText - выводит текст ошибки, если поле обязательно к
// заполнению и не было заполненно;
// А также такие методы как:

// render - возвращает HTML-разметку формы;
// handleBlur - срабатывает, если поле обязательно к заполнению и не было
// заполнено.

// Также напишите класс Form, который будет создавать HTML-форму, и у которого
// будут такие параметры:

// id;
// классы;
// action;

// А также четыре метода:

// render - который создает каркас формы и возвращает его;
// handleSumbit - который отвечает за обработку отправки формы;
// serialize, который все заполненые поля формы собирает в строку формата:
// "nameПоля=значениеПоля";
// serializeJSON, который все заполненные поля формы собирает в объект, ключами
// которого будут значения name

class Modal {
  constructor(id, classes, text) {
    this.id = id;
    this.classes = classes;
    this.text = text;
  }
  render = function (content = "") {
    this.window = document.createElement("div");
    this.window.id = this.id;
    this.window.classList.add(...this.classes);
    const modalContent = document.createElement("div");
    modalContent.classList.add("modal-content");
    const span = document.createElement("span");
    span.classList.add("close");
    span.innerHTML = "&times;";
    modalContent.append(span, content);
    this.window.append(modalContent);

    span.addEventListener("click", this.closeModal.bind(this));

    return this.window;
  };
  openModal = function () {
    this.window.classList.add("active");
  };
  closeModal = function () {
    this.window.classList.remove("active");
  };
}

class Registration extends Modal {
  constructor(id, classes, text) {
    super(id, classes, text);
  }
  createForm = function () {
    const newRegForm = new RegisterForm("form", "form", "blank", {
      type: "submit",
      name: "submitBtn",
      required: false,
      id: "submitBtn",
      classes: "page-input",
      placeHolder: "Регистрация",
      textError: "ERROR",
    });
    return newRegForm.render(newRegForm.renderFormContent());
  };
}

class Login extends Modal {
  constructor(id, classes, text) {
    super(id, classes, text);
  }
  createForm = function () {
    const loginForm = new LoginForm("loginForm", "login-form", "", {
      type: "submit",
      name: "submitBtn",
      required: false,
      id: "submitBtn",
      classes: "page-input",
      placeHolder: "Логин",
      textError: "ERROR",
    });
    return loginForm.render(loginForm.renderFormContent());
  };
}

class Input {
  constructor(type, name, required, id, classes, placeHolder, textError) {
    this.type = type;
    this.name = name;
    this.required = required;
    this.id = id;
    this.classes = classes;
    this.placeHolder = placeHolder;
    this.textError = textError;
  }
  render() {
    this.input = document.createElement("input");
    this.input.setAttribute("type", this.type);
    this.input.setAttribute("name", this.name);
    if (this.required === true) {
      this.input.setAttribute("required", "");
    }
    this.input.setAttribute("id", this.id);
    this.input.setAttribute("class", this.classes);
    if (this.type === "submit") {
      this.input.setAttribute("value", this.placeHolder);
    } else {
      this.input.setAttribute("placeholder", this.placeHolder);
    }
    this.input.addEventListener("blur", this.handleBlur);
    return this.input;
  }
  handleBlur() {
    if (this.value.trim() === "") {
      alert("Full field");
    }
  }
}
class Form {
  constructor(id, classes, action, submitProps) {
    this.id = id;
    this.classes = classes;
    this.action = action;
    this.submitProps = submitProps;
  }
  render(content) {
    const { type, name, required, id, classes, placeHolder, textError } =
      this.submitProps;
    this.form = document.createElement("form");
    this.form.setAttribute("id", this.id);
    this.form.setAttribute("class", this.classes);
    this.form.setAttribute("action", this.action);
    const inputSubmit = new Input(
      type,
      name,
      required,
      id,
      classes,
      placeHolder,
      textError
    ).render();
    this.form.append(content, inputSubmit);
    this.form.addEventListener("submit", (event) => {
      event.preventDefault();
      this.handleSumbit(this.form);
      console.log(this.serialize());
      console.log(this.serializeJSON());
    });
    return this.form;
  }
  handleSumbit() {
    alert("Ваши данные отправленны!");
  }
  serialize() {
    let result = [...this.form.elements].reduce((accu, input) => {
      if (input.type !== "submit") {
        accu += input.name + "=" + input.value + "&";
      }
      return accu;
    }, "");
    return result.slice(0, result.length - 1);
  }
  serializeJSON() {
    return [...this.form.elements].reduce((accu, input) => {
      if (input.type !== "submit") {
        accu[input.name] = input.value;
      }
      return accu;
    }, {});
  }
}

class RegisterForm extends Form {
  constructor(id, classes, action, submitProps) {
    super(id, classes, action, submitProps);
  }
  renderFormContent() {
    const inputLogin = new Input(
      "text",
      "login",
      true,
      "regLogin",
      "page-input",
      "Ваш логин",
      "ERROR"
    );
    const inputEmail = new Input(
      "email",
      "email",
      true,
      "regEmail",
      "page-input",
      "Ваш Email",
      "ERROR"
    );
    const inputPassword = new Input(
      "password",
      "password",
      true,
      "regPassword",
      "page-input",
      "Ваш пароль",
      "ERROR"
    );
    const inputRepeatPassword = new Input(
      "password",
      "repeat-password",
      true,
      "regrepeatPass",
      "page-input",
      "Повторите  пароль",
      "ERROR"
    );
    const fragment = document.createDocumentFragment();
    fragment.append(
      inputLogin.render(),
      inputEmail.render(),
      inputPassword.render(),
      inputRepeatPassword.render()
    );
    return fragment;
  }
  handleSumbit(form) {
    if (
      form.querySelector("#regPassword").value ===
      form.querySelector("#regrepeatPass").value
    ) {
      super.handleSumbit();
    }
  }
}

class LoginForm extends Form {
  constructor(id, classes, action, submitProps) {
    super(id, classes, action, submitProps);
  }
  renderFormContent() {
    const inputLogin = new Input(
      "text",
      "login",
      true,
      "regLogin",
      "page-input",
      "Ваш логин",
      "ERROR"
    );
    const inputPassword = new Input(
      "password",
      "password",
      true,
      "regPassword",
      "page-input",
      "Ваш пароль",
      "ERROR"
    );
    const fragment = document.createDocumentFragment();
    fragment.append(inputLogin.render(), inputPassword.render());
    return fragment;
  }
  handleSumbit(form) {
    if (
      form.querySelector("#regLogin").value.trim() !== "" &&
      form.querySelector("#regPassword").value.trim() !== ""
    ) {
      super.handleSumbit();
    }
  }
}

const root = document.querySelector("#root");
const modal = new Modal("window", ["modal", "lorem"], "lorem ipsum dolot");
root.append(modal.render());

const btn = document.querySelector("#myBtn");
btn.addEventListener("click", modal.openModal.bind(modal));

const registrationForm = new Registration(
  "window",
  ["modal", "lorem"],
  "lorem ipsum dolot"
);
const registerBtn = document.querySelector("#registration-btn");
registerBtn.addEventListener(
  "click",
  registrationForm.openModal.bind(registrationForm)
);
root.append(registrationForm.render(registrationForm.createForm()));

const loginForm = new Login("window", ["modal", "lorem"], "lorem ipsum dolot");
const loginBtn = document.querySelector("#login-btn");
loginBtn.addEventListener("click", loginForm.openModal.bind(loginForm));
root.append(loginForm.render(loginForm.createForm()));
