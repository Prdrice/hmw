import { MoviesCard } from "./moviecard.js";
import { CharacterList } from "./listChr.js";

fetch(`https://ajax.test-danit.com/api/swapi/films`)
  .then((response) => response.json())
  .then((data) => {
    console.log(data);
    data.forEach(
      ({ id, episodeId, characters: [...url], name, openingCrawl }) => {
        new MoviesCard(id, episodeId, name, openingCrawl).render();

        const promiseArr = [...url].map((url) =>
          fetch(url).then((response) => response.json())
        );

        console.log(promiseArr);
        Promise.allSettled([...promiseArr]).then((arrCharacters) => {
          const arrName = arrCharacters.map((el) => el.value.name);
          console.log(arrName);
          arrName.forEach((el) => {
            new CharacterList(el, id).building();
          });
        });
      }
    );
});
