import { CharacterList } from "./listChr.js";

export class MoviesCard {
  constructor(id, episodeId, name, openingCrawl) {
    this.episodeId = episodeId;
    this.name = name;
    this.openingCrawl = openingCrawl;
    this.id = id;
  }
  render() {
    const divChar = document.createElement("div");
    divChar.classList.add(`class${this.id}`);
    divChar.style.cssText = 
    `border: 2px solid #e636d3;
     border-radius: 10px;
     color: #3444d6;
     padding: 15px;
     margin: 5px 50px 5px 50px;`;

    const ulChar = document.createElement("ul");
    document.body.append(divChar);

    divChar.insertAdjacentHTML(
      `beforeend`,
      `
      <h2>Зоряні війни</h2>
      <h3>Епізод ${this.episodeId}</h3>
      <h4>${this.name} </h4>
      <p> ${this.openingCrawl}</p>                              
      `
    );
    divChar.append(ulChar);
    ulChar.setAttribute("id", `${this.id}`);
  }
}
