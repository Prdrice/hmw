import { MoviesCard } from "./moviecard.js";


export class CharacterList {
  constructor(e, id) {
    this.e = e;
    this.id = id;
  }
  building() {
    const ul = document.getElementById(`${this.id}`);
    ul.insertAdjacentHTML(
      `beforeend`,
      `<li>${this.e}</li>`
    );
  }
}
