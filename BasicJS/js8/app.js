"use strickt";

//1
let paragraph = document.querySelectorAll('p');
console.log(paragraph);
paragraph.forEach((p) => p.style = 'background-color: #ff0000');

//2
let idElement = document.getElementById('optionsList');
console.log(idElement.parentElement);
for (let child of idElement.childNodes) {
  console.log(child.nodeType);
  console.log(child.nodeName);
}

//3
const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerText = 'This is a paragraph';

//4
const mainHeader = document.querySelectorAll('.main-header li');
console.log(mainHeader);
for (const item of mainHeader) {
  item.classList.add('nav-item')
}

//5
const sectionTitles = document.querySelectorAll('.section-title');
sectionTitles.forEach((item) => item.classList.remove('section-title'));
console.log(sectionTitles);

//1.DOM- это объектное представление исходного HTML-кода документа, браузер получает HTML-код, парсит его и строит DOM 
//2.innerText - показывает всё текстовое содержимое, которое не относится к синтаксису HTML без тегов.innerHTML - покажет текстовую информацию ровно по одному элементу с тегами html.
//3.Можно обратиться через document.querySelectorAll , document.querySelector , document.getElementsByName , document.getElementById , document.getElementsByClassName ... лучший document.querySelectorAll но если нужно вернуть первый элемент из определенного селектора то используем document.querySelector


