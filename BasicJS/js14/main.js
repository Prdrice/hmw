"use strickt";

document.querySelectorAll('.centered-content li').forEach(li => {
    li.addEventListener('click', (event) => {
        document.querySelector('.active')?.classList.remove('active');
        event.target.classList.add('active');

        document.querySelector('.action-text')?.classList.remove('action-text');
        document.querySelectorAll('.tabs-content li').forEach(li => {
            if (li.dataset.id === event.target.dataset.id) {
                li.classList.add('action-text');
            }
        })
    })
})

//14
document.querySelector('.toggle-theme').addEventListener('click', (event) =>{
    event.preventDefault();
    if (localStorage.getItem('theme') === 'dark') {
      localStorage.removeItem('theme');
    }
    else {
      localStorage.setItem('theme', 'dark')
    }
    addDarkClassToHTML();
    });
    
    function addDarkClassToHTML() {
    try {
      if (localStorage.getItem('theme') === 'dark') {
        document.querySelector('html').classList.add('dark');
        document.querySelector('.toggle-theme span').textContent = 'toggle_on';
      }
      else {
        document.querySelector('html').classList.remove('dark');
        document.querySelector('.toggle-theme span').textContent = 'toggle_off';
      }
    } catch(err) {}
    }
    
    addDarkClassToHTML();
    







