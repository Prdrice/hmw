"use strickt";

let arr = ['hello', 'world', 23, '23', null];

const filterBy = (array, type) => {
    return array.reduce((res, currentItem) => { 
        if ( typeof currentItem != type) {  
            res.push(currentItem);
        } 
        return res} , []); 
}

 console.log(filterBy(arr, 'string'))
 console.log(filterBy(arr, 'number'));
 console.log(filterBy(arr, 'boolean'));
 console.log(filterBy(arr, 'object'));


//1. forEach() выполняет указанную функцию 1 раз для каждого элемента в массиве
//2. array = []; , arr.length = 0; то есть установить длину 0
//3. способом arr.isArray()



