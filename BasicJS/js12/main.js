"use strickt";

const btn = document.querySelectorAll(".btn");

document.addEventListener("keyup", (event) => {
  btn.forEach((e) => {
    if (event.key.toLowerCase() == e.textContent.toLowerCase()) {
      e.classList.toggle("active");
    } else {
      e.classList.remove("active");
    }
  });
});


//потому что, разные операционные системы считыват информацию по разному, это может привести к вводу не правильной информации, так же пользователи могут вносить данные голосом, из-за чего обработка с помощью клавищи будет не возможна.