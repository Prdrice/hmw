"use strickt";

const check = document.querySelectorAll(".icon-password")
const inputPassword = document.querySelector(".input-password")
const submit = document.querySelector(".btn")
const inputSubmit = document.querySelector(".input-submit")
const messageError = document.createElement("p")
messageError.innerText = "Нужно ввести одинаковые значения"
messageError.style.color = "#ed2939"; 

check.forEach(e => { 
    e.addEventListener("click", (event) => {
        let elInput = event.target.parentElement.children[0]
        if(elInput.getAttribute("type") === "password"){
            event.target.classList.toggle("fa-eye-slash", false)
            event.target.classList.toggle("fa-eye", true)
            elInput.setAttribute("type", "text")    
        }        
        else{
            event.target.classList.toggle("fa-eye-slash", true)
            event.target.classList.toggle("fa-eye", false)
            elInput.setAttribute("type", "password")
        }
    })
})
submit.addEventListener("click", (event) => {
    event.preventDefault() 
    messageError.remove()
    if(inputPassword.value === inputSubmit.value) {
        alert("You are welcom");
        return;
    }
    else{          
        inputSubmit.after(messageError)
    }
})
