"use strickt";

document.querySelectorAll('.centered-content li').forEach(li => {
    li.addEventListener('click', (event) => {
        document.querySelector('.active')?.classList.remove('active');
        event.target.classList.add('active');

        document.querySelector('.action-text')?.classList.remove('action-text');
        document.querySelectorAll('.tabs-content li').forEach(li => {
            if (li.dataset.id === event.target.dataset.id) {
                li.classList.add('action-text');
            }
        })
    })
})







