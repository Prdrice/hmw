"use strickt";

const buttonStop = document.querySelector(".stop");
const buttonStart = document.querySelector(".start");
const img = [...document.querySelectorAll("img")];

let leftImg = 0;
let positionImg = 0;
let sliderTimer;
start();

function slider() {
  img[positionImg].classList.remove("image-to-show_active");
  img[positionImg].classList.add("image-to-show");
  leftImg++;

  if (leftImg > img.length - 1) leftImg = 0;
  img[leftImg].classList.remove("image-to-show");
  img[positionImg].classList.add("image-to-show_active");
  positionImg = leftImg;
}

function stop() {
  clearInterval(sliderTimer);
  sliderTimer = null;
}
function start() {
  if (!sliderTimer) {
    sliderTimer = setInterval(slider, 3000);
  }
}
buttonStop.addEventListener("click", stop);
buttonStart.addEventListener("click", start);

//1.setTimeout() -позволяет вызвать функцию один раз через определённый интервал времени
//setInterval() -позволяет вызывать функцию регулярно, повторяя вызов через определённый интервал времени
//2.у setTimeout() задача выполнить переданную функцию через задонное время, из-за этого если передать нулевую задержку ничего не случиться
//3.clearInterval() - отменяет повторение действий которые установленные были через setInterval(), эсли это не сделать функция не остановиться.