"use strickt";

let newList = ["Earth", "Mercury", "Venus", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"];

let showList = (arr) => {
    let ulList = document.createElement('ul');
    const massive = arr.map((items) => {
        let lisItem = `
        <li>${items}</li>`;
        return lisItem
    })
    ulList.innerHTML = massive.join("");
    return ulList
}
document.querySelector("body").after(showList(newList));


//1. document.createElement('e') и document.body.innerHTML = 'e'
//2. insertAdjacentHTML - Это универсальный способ вставить элемент на страницу, если он представлен в виде готовой HTML строки.
// "beforebegin" – вставить html перед elem,
// "afterbegin" – вставить html в начало elem,
// "beforeend" – вставить html в конец elem,
// "afterend" – вставить html после elem.
//3. Для удаления элемента со страницы можно вызвать метод `remove()`




