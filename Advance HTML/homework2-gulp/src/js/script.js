"use strict";

let menu = document.querySelector(".nav__menu");
let buttonClose = document.querySelector(".nav__burger-close");
const buttonOpen = document.querySelector(".nav__burger");
const mediaQuery = window.matchMedia("(max-width: 767px)");
if (mediaQuery.matches) {
  buttonOpen.addEventListener("click", function () {
    menu.classList.add("nav__menu-active");
    this.style.display = "none";
    buttonClose.style.display = "block";
  });

  buttonClose.addEventListener("click", function () {
    this.style.display = "none";
    menu.classList.remove("nav__menu-active");
    buttonOpen.style.display = "flex";
  });
}
