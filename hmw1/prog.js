import { Employee } from "./employe.js";

export class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return super.salary * 3;
  }
}

