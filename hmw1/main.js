import {Programmer} from "./prog.js";


const prog1 = new Programmer("Rose", 25, 1800, "react");
const prog2 = new Programmer("Sam", 19, 900, "js");
const prog3 = new Programmer("Eddi", 32, 3000, "react native");

console.log(prog1);
console.log(prog1.salary);
console.log(prog2);
console.log(prog2.salary);
console.log(prog3);
console.log(prog3.salary);
