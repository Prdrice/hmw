
class UserInfo {
  constructor(continent, country, regionName, city) {
    this.continent = continent;
    this.country = country;
    this.regionName = regionName;
    this.city = city;
  }
  render() {
    const div = document.querySelector(".container");
    div.innerHTML = "";
    div.insertAdjacentHTML(
      "beforeend",
      `<div>
    <h3>Are you here?</h3>
    <p>${this.continent}</p>
    <p>${this.country}</p>
    <p>${this.regionName}</p>
    <p>${this.city}</p>
   </div>`
    );
  }
}

