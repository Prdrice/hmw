"use strict";

const button = document.querySelector(`.button`);
console.log(button);
button.addEventListener("click", () => {
  (() => {
    const div = document.querySelector(".container");
    div.insertAdjacentHTML(
      "beforeend",
      `
        <h3> Maybe...</h3>`
    );
  })();

  try {
    (async () => {
      const ipUser = await fetch("https://api.ipify.org/?format=json").then(
        (response) => response.json()
      );
      const { ip } = ipUser;
      console.log(ip);
      const url = `http://ip-api.com/json/${ip}`;
      console.log(url);
      const user = async (ip) => {
        const data = await fetch(`http://ip-api.com/json/${ip}`).then(
          (response) => response.json()
        );
        const { continent = `Europe?`, country, regionName, city } = data;
        new UserInfo(continent, country, regionName, city).render();
      };
      user(ip);
    })();
  } catch (error) {
    console.warn("err");
  }
});
